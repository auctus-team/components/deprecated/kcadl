# Continous integration
[![pipeline status](https://gitlab.inria.fr/auctus/kcadl/badges/master/pipeline.svg)](https://gitlab.inria.fr/auctus/kcadl/commits/master)
[![Quality Gate](https://sonarqube.inria.fr/sonarqube/api/badges/gate?key=auctus:kcadl)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:kcadl)
[![Coverage](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:kcadl&metric=coverage)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:kcadl)

[![Bugs](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:kcadl&metric=bugs)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:kcadl)
[![Vulnerabilities](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:kcadl&metric=vulnerabilities)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:kcadl)
[![Code smells](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:kcadl&metric=code_smells)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:kcadl)

[![Line of code](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:kcadl&metric=ncloc)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:kcadl)
[![Comment ratio](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:kcadl&metric=comment_lines_density)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:kcadl)

# Links
- Sonarqube : https://sonarqube.inria.fr/sonarqube/dashboard?id=auctus%3Akcadl
- Documentation : https://auctus.gitlabpages.inria.fr/kcadl

# Installation
Clone repository
```bash
https://gitlab.inria.fr/auctus/kcadl.git
```

Install 3rd party libraries
```bash
./install
```

Build kcadl library
```bash
mkdir build
cd build
cmake ../
make 
sudo make install
```

# Examples
To build kcadl examples
```bash
cd kcadl/examples
make 
```

To run kcadl examples
```bash
cd kcadl
./examples/Forward-Kinematics/BarrettTech_WAM_FK
```
