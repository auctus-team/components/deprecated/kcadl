Introduction {#mainpage}
=========

A library for modeling kinematic chains that may have variabilities and uncertainties.

Available on Gitlab: https://gitlab.inria.fr/auctus/kcadl


