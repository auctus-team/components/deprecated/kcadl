Frank Panda Predictions {#page3}
=========

# How to run predictions

Start the ROS prediction nodes as follows:
```bash
source devel/setup.bash
roslaunch franka_panda_prediction predictions.launch
```

# Visualizations (Debug mode)

Additional data can be exported for visualization with Matlab using the debug mode as follows:
```bash
source devel/setup.bash
roslaunch franka_panda_prediction predictions_debug.launch
```
Matlab tools are found in utilities/Matlab.


# Frank Panda Braking Force Prediction

This describes the precedure for starting the ROS node "Franka_Panda_Braking_Force_Prediction".
This node predicts the braking force of the robot a given time-step and returns the force values in Newtons.

Start the ROS node as follows:
```bash
source devel/setup.bash
roslaunch franka_panda_prediction franka_panda_braking_force_prediction.launch
```
Example topic:
```bash
rostopic pub /prediction_braking_force_input franka_panda_prediction/PredictionBrakingForceInput "tau: 0.01" 
```

# Franka Panda and Human Distance Prediction

This describes the precedure for starting the ROS node "Franka_Panda_Human_Distance_Prediction".
This node predicts the future motion of the human over a given time-step and returns the minimum distance between the human and robot during that time step. 

Start the ROS node as follows:
```bash
source devel/setup.bash
roslaunch franka_panda_prediction franka_panda_human_distance_prediction.launch 
```

Example topic:
```bash
rostopic pub /prediction_distance_input franka_panda_prediction/PredictionDistanceInput "
Ps: {x: 0.2, y: 0.4, z: 0.0}
Pe: {x: 0.2, y: 0.4, z: 0.2874}
Pw: {x: 0.2, y: 0.6715, z: 0.2874}
Ph: {x: 0.2, y: 0.6715, z: 0.325}
Ps_dot: {x: 0.2, y: 0.2, z: 0.2}
Pe_dot: {x: 0.3, y: 0.3, z: 0.3}
Pw_dot: {x: 0.4, y: 0.4, z: 0.4}
Ph_dot: {x: 0.5, y: 0.5, z: 0.5}
lu: 0.2874
lf: 0.2715
lh: 0.0376
rad_lu: 0.05
rad_lf: 0.035
rad_lh: 0.05
err_lu: 0.02
err_lf: 0.02
err_lh: 0.02
err_HP: 0.01
err_RP: 0.01
tau: 0.1
allowdist: 0.2"
```

