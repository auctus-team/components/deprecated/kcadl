Installation {#page}
=========

Tested on Ubuntu 16.04, Ubuntu 18.04

To install run:

```bash
git clone https://gitlab.inria.fr/auctus/kcadl
cd kcadl
./install.sh
```

