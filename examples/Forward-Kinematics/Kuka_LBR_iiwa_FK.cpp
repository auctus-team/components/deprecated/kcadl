/*
 * Kuka_LBR_iiwa_FK.cpp
 *
 *  Created on: Nov 13, 2018
 *  Author: Joshua Pickard
 */

#include "kcadl.h"

using namespace kcadl;

int main() {
	// Kuka LBR iiwa manipulator
	ibex::Interval r1 = 0.360;
	ibex::Interval r2 = 0.420;
	ibex::Interval r3 = 0.400;
	ibex::Interval r4 = 0.126;

	// Create twists from variables (screw axes in base frame)
	ibex::Variable v_0("var_0");
	ibex::Variable v_r1("var_r1");
	ibex::Variable v_r2("var_r2");
	ibex::Variable v_r3("var_r3");
	ibex::Variable v_r4("var_r4");
	ibex::Function varTwist1(v_0,v_r1,v_r2,v_r3,v_r4,
			ibex::Return(v_0,v_0,v_0,v_0,v_0,v_0+1));
	ibex::Function varTwist2(v_0,v_r1,v_r2,v_r3,v_r4,
			ibex::Return(-v_r1,v_0,v_0,v_0,v_0-1,v_0));
	ibex::Function varTwist3(v_0,v_r1,v_r2,v_r3,v_r4,
			ibex::Return(v_0,v_0,v_0,v_0,v_0,v_0+1));
	ibex::Function varTwist4(v_0,v_r1,v_r2,v_r3,v_r4,
			ibex::Return(v_r1+v_r2,v_0,v_0,v_0,v_0+1,v_0));
	ibex::Function varTwist5(v_0,v_r1,v_r2,v_r3,v_r4,
			ibex::Return(v_0,v_0,v_0,v_0,v_0,v_0+1));
	ibex::Function varTwist6(v_0,v_r1,v_r2,v_r3,v_r4,
			ibex::Return(-v_r1-v_r2-v_r3,v_0,v_0,v_0,v_0-1,v_0));
	ibex::Function varTwist7(v_0,v_r1,v_r2,v_r3,v_r4,
			ibex::Return(v_0,v_0,v_0,v_0,v_0,v_0+1));
	ibex::IntervalVector allVarsVector(5);
	allVarsVector[0] = ibex::Interval(0);
	allVarsVector[1] = ibex::Interval(r1);
	allVarsVector[2] = ibex::Interval(r2);
	allVarsVector[3] = ibex::Interval(r3);
	allVarsVector[4] = ibex::Interval(r4);
	Twist twist1(varTwist1,allVarsVector);
	Twist twist2(varTwist2,allVarsVector);
	Twist twist3(varTwist3,allVarsVector);
	Twist twist4(varTwist4,allVarsVector);
	Twist twist5(varTwist5,allVarsVector);
	Twist twist6(varTwist6,allVarsVector);
	Twist twist7(varTwist7,allVarsVector);

	// Create segments
	Segment segmentBase(Joint(Joint::JointType::Base),Frame::Identity());
	Segment segment1(Joint(Joint::JointType::Revolute),Frame::From_twist(twist1));
	Segment segment2(Joint(Joint::JointType::Revolute),Frame::From_twist(twist2));
	Segment segment3(Joint(Joint::JointType::Revolute),Frame::From_twist(twist3));
	Segment segment4(Joint(Joint::JointType::Revolute),Frame::From_twist(twist4));
	Segment segment5(Joint(Joint::JointType::Revolute),Frame::From_twist(twist5));
	Segment segment6(Joint(Joint::JointType::Revolute),Frame::From_twist(twist6));
	Segment segment7(Joint(Joint::JointType::Revolute),Frame::From_twist(twist7));

	// Build kinematic chain
	KinematicChain kinematicChain;
	kinematicChain.verbose = false; // Do not display debugging comments
	kinematicChain.addSegment(segmentBase);
	kinematicChain.addSegment(segment1);
	kinematicChain.addSegment(segment2);
	kinematicChain.addSegment(segment3);
	kinematicChain.addSegment(segment4);
	kinematicChain.addSegment(segment5);
	kinematicChain.addSegment(segment6);
	kinematicChain.addSegment(segment7);

	// Base transformation
	std::cout << "Compute transform" << std::endl;
	ibex::IntervalMatrix rotationMatrix = ibex::Matrix::eye(3);
	ibex::IntervalVector positionVector(3);
	positionVector[0] = 0;
	positionVector[1] = 0;
	positionVector[2] = r1+r2+r3+r4;
	kinematicChain.zeroTransform(positionVector, rotationMatrix);

	// Find FK solutions - given joint values
	ibex::IntervalVector jointValues(7);
	jointValues[0] = 0;
	jointValues[1] = 0;
	jointValues[2] = 0;
	jointValues[3] = 0;
	jointValues[4] = 0;
	jointValues[5] = 0;
	jointValues[6] = 0;
	bool use_filtering = true;
	bool base_reference = true;
	std::vector<KinematicChain::KinematicsSolution> solutions1 = kinematicChain.solveFK(jointValues, use_filtering, base_reference);

	// Display FK results
	for (int index=0; index<solutions1.size(); index++){
		ibex::IntervalMatrix T = Frame::Position_and_orientation(solutions1[index].eePosition,solutions1[index].eeOrientation).TransformationMatrix();
		std::cout << "Homogenous Transform\n" << T << std::endl;
	}

	std::cout << " Finished" << std::endl;
	return 0;
}

