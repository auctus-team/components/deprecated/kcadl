/*
 * Human_7dof_upperlimb_FK.cpp
 *
 *  Created on: Jan 8, 2019
 *  Author: Joshua Pickard
 */

#include "kcadl.h"

using namespace kcadl;

int main() {
	// Human upper limb model
	ibex::Interval lu = 28.74; //upperarm length
	ibex::Interval lf = 27.15; //forearm length
	ibex::Interval lh = 3.76;  //hand length

	// Create twists (screw axes in base frame)
	Twist twist1(0,0,0,0,0,1);
	Twist twist2(0,0,0,0,-1,0);
	Twist twist3(0,0,0,0,0,1);
	Twist twist4(lu,0,0,0,-1,0);
	Twist twist5(0,0,0,0,0,1);
	Twist twist6(lf+lu,0,0,0,-1,0);
	Twist twist7(0,0,0,0,0,1);
	Twist twist8(0,0,0,0,0,1);

	// Create segments
	Segment segmentBase(Joint(Joint::JointType::Base),Frame::Identity());
	Segment segment1(Joint(Joint::JointType::Revolute),Frame::From_twist(twist1));
	Segment segment2(Joint(Joint::JointType::Revolute),Frame::From_twist(twist2));
	Segment segment3(Joint(Joint::JointType::Revolute),Frame::From_twist(twist3));
	Segment segment4(Joint(Joint::JointType::Revolute),Frame::From_twist(twist4));
	Segment segment5(Joint(Joint::JointType::Revolute),Frame::From_twist(twist5));
	Segment segment6(Joint(Joint::JointType::Revolute),Frame::From_twist(twist6));
	Segment segment7(Joint(Joint::JointType::Revolute),Frame::From_twist(twist7));
	Segment segment8(Joint(Joint::JointType::Revolute),Frame::From_twist(twist8));

	// Build kinematic chain
	KinematicChain kinematicChain;
	kinematicChain.verbose = false; // Do not display debugging comments
	kinematicChain.addSegment(segmentBase);
	kinematicChain.addSegment(segment1);
	kinematicChain.addSegment(segment2);
	kinematicChain.addSegment(segment3);
	kinematicChain.addSegment(segment4);
	kinematicChain.addSegment(segment5);
	kinematicChain.addSegment(segment6);
	kinematicChain.addSegment(segment7);
	kinematicChain.addSegment(segment8);

	// Zero transform
	ibex::IntervalMatrix rotationMatrix = ibex::Matrix::eye(3);
	ibex::IntervalVector positionVector(3);
	positionVector[0] = 0;
	positionVector[1] = 0;
	positionVector[2] = lu+lf+lh;
	kinematicChain.zeroTransform(positionVector, rotationMatrix);

	// Find FK solutions - given joint values
	ibex::IntervalVector jointValues(8,0.5 + ibex::Interval(-0.01,0.01));
	jointValues[7] = 0; // not actuated
	bool use_filtering = true;
	bool base_reference = true;
	std::vector<KinematicChain::KinematicsSolution> solutions1 = kinematicChain.solveFK(jointValues, use_filtering, base_reference);

	// Display FK results
	for (int index=0; index<solutions1.size(); index++){
		ibex::IntervalMatrix T = Frame::Position_and_orientation(solutions1[index].eePosition,solutions1[index].eeOrientation).TransformationMatrix();
		std::cout << "Homogenous Transform\n" << T << std::endl;
	}

	std::cout << " Finished" << std::endl;
	return 0;
}











