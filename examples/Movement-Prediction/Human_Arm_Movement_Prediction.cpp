/*
 * Human_Arm_Movement_Prediction.cpp
 *
 *  Created on: Jun 18, 2019
 *  Author: Joshua Pickard
 */

#include "kcadl.h"
#include <math.h>

using namespace kcadl;

int main() {

	// Segment lengths
	ibex::Interval lu = 28.74;
	ibex::Interval lf = 27.15;
	ibex::Interval lh = 3.76;

	// Joint angles
	ibex::Interval theta1(0,0.1);
	ibex::Interval theta2(0,0.1);
	ibex::Interval theta3(0,0.1);
	ibex::Interval theta4(0,0.1);
	ibex::Interval theta5(0,0.1);
	ibex::Interval theta6(0,0.1);
	ibex::Interval theta7(0,0.1);

	//#######################################
	// Build a kinematic chain
	//#######################################
	// Create frames
	Frame frame01 = Frame::modifiedDH(0,M_PI/2,0,theta1);
	Frame frame12 = Frame::modifiedDH(0,M_PI/2,0,theta2);
	Frame frame23 = Frame::modifiedDH(0,-M_PI/2,lu,theta3);
	Frame frame34 = Frame::modifiedDH(0,M_PI/2,0,theta4);
	Frame frame45 = Frame::modifiedDH(0,-M_PI/2,lf,theta5);
	Frame frame56 = Frame::modifiedDH(0,M_PI/2,0,theta6+M_PI/2);
	Frame frame67 = Frame::modifiedDH(0,M_PI/2,0,theta7-M_PI/2);
	Frame frame78 = Frame::modifiedDH(0,-M_PI/2,lh,-M_PI/2);

	// Add segments to chain
	KinematicChain kinematicChain;
	kinematicChain.verbose = false; // Do not display debugging comments
	kinematicChain.addSegment(Segment(Joint(Joint::JointType::Revolute),frame01));
	kinematicChain.addSegment(Segment(Joint(Joint::JointType::Revolute),frame12));
	kinematicChain.addSegment(Segment(Joint(Joint::JointType::Revolute),frame23));
	kinematicChain.addSegment(Segment(Joint(Joint::JointType::Revolute),frame34));
	kinematicChain.addSegment(Segment(Joint(Joint::JointType::Revolute),frame45));
	kinematicChain.addSegment(Segment(Joint(Joint::JointType::Revolute),frame56));
	kinematicChain.addSegment(Segment(Joint(Joint::JointType::Revolute),frame67));
	kinematicChain.addSegment(Segment(Joint(Joint::JointType::Revolute),frame78));

	//#######################################
	// Frame products
	//#######################################
	frame01.Update_DH_theta(ibex::Interval(0.2,0.3));
	frame12.Update_DH_theta(ibex::Interval(0.2,0.3));
	frame23.Update_DH_theta(ibex::Interval(0.2,0.3));
	frame34.Update_DH_theta(ibex::Interval(0.2,0.3));
	frame45.Update_DH_theta(ibex::Interval(0.2,0.3));
	frame56.Update_DH_theta(ibex::Interval(0.2,0.3));
	frame67.Update_DH_theta(ibex::Interval(0.2,0.3));

//	std::cout << frame01 << std::endl;
	Frame frame02 = frame01 * frame12;
//	std::cout << frame02 << std::endl;
	Frame frame03 = frame02 * frame23;
//	std::cout << frame03 << std::endl;
	Frame frame04 = frame03 * frame34;
//	std::cout << frame04 << std::endl;
	Frame frame05 = frame04 * frame45;
//	std::cout << frame05 << std::endl;
	Frame frame06 = frame05 * frame56;
//	std::cout << frame06 << std::endl;
	Frame frame07 = frame06 * frame67;
//	std::cout << frame07 << std::endl;
	Frame frame08 = frame07 * frame78;
//	std::cout << frame08 << std::endl;

	std::cout << frame01.position << std::endl;
	std::cout << frame03.position << std::endl;
	std::cout << frame05.position << std::endl;
	std::cout << frame08.position << std::endl;

}


