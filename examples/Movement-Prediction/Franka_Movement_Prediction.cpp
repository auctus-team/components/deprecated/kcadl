/*
 * Franka_Movement_Prediction.cpp
 *
 *  Created on: Jun 19, 2019
 *  Author: Joshua Pickard
 *
 * Description:
 * Estimates the distance between predicted FRANKA motion and predicted human upper-limb
 * motion for a given prediction horizon.
 *
 * Details:
 * All links are modeled as cylinders with hemispherical ends. Link end-point
 * positions during a time range of [0,tau] are modeled as boxes (interval vectors)
 * and the set of link center-line locations are described by the convex hull of link
 * end-point boxes (link sets). The minimum distance between all combinations of robot
 * and human link sets minus link radii gives the predicted minimum distance between
 * the robot and human during the time range.
 */

#include "kcadl.h"
#include <vector>
#include <math.h>
#include <chrono>
#include <iostream>
#include <fstream>

using namespace kcadl;

void writeBox(std::ofstream &os, ibex::IntervalVector box);

int main(int argc, char** argv)  {
	// Start time
	auto start = std::chrono::high_resolution_clock::now();

	//################################################
	// Values to be received from ROS
	//################################################
	// FRANKA values
	double theta1_ = 0.0;
	double theta2_ = 0.0;
	double theta3_ = 0.0;
	double theta4_ = -0.5;
	double theta5_ = 0.0;
	double theta6_ = 2.0;
	double theta7_ = 0.0;
	double theta1_dot = 0.3;
	double theta2_dot = 0.3;
	double theta3_dot = 0.3;
	double theta4_dot = 0.3;
	double theta5_dot = 0.3;
	double theta6_dot = 0.3;
	double theta7_dot = 0.3;
	double theta1_ddot = 0.1;
	double theta2_ddot = 0.1;
	double theta3_ddot = 0.1;
	double theta4_ddot = 0.1;
	double theta5_ddot = 0.1;
	double theta6_ddot = 0.1;
	double theta7_ddot = 0.1;
	double err_RP = 0.01;

	// HUMAN values
	double Ps[3] = {0.2,0.4,0};
	double Pe[3] = {0.2,0.4,0.2874};
	double Pw[3] = {0.2,0.4+0.2715,0.2874};
	double Ph[3] = {0.2,0.4+0.2715,0.2874+0.0376};
	double Ps_dot[3] = {0.2,0.2,0.2};
	double Pe_dot[3] = {0.3,0.3,0.3};
	double Pw_dot[3] = {0.4,0.4,0.4};
	double Ph_dot[3] = {0.5,0.5,0.5};
	double lu = 0.2874;
	double lf = 0.2715;
	double lh = 0.0376;
	double rad_lu = 0.05;
	double rad_lf = 0.035;
	double rad_lh = 0.05;
	double err_lu = 0.02;
	double err_lf = 0.02;
	double err_lh = 0.02;
	double err_HP = 0.01;

	// Prediction horizon (s)
	double tau = 0.1;
	double allowdist = 0.2;

	//################################################
	// Determine prediction time
	//################################################
	// Prediction time interval (s)
	ibex::Interval pred_time(0,tau);

	//######################################################
	// Determine future range of joint values of the FRANKA
	//######################################################
	// Segment lengths (m)
	ibex::Interval d1 = 0.333 + ibex::Interval(-0.001,0.001);
	ibex::Interval d3 = 0.316 + ibex::Interval(-0.001,0.001);
	ibex::Interval d5 = 0.384 + ibex::Interval(-0.001,0.001);
	ibex::Interval d8 = 0.107 + ibex::Interval(-0.001,0.001);
	ibex::Interval a4 = 0.0825 + ibex::Interval(-0.001,0.001);
	ibex::Interval a5 = -0.0825 + ibex::Interval(-0.001,0.001);
	ibex::Interval a7 = 0.088 + ibex::Interval(-0.001,0.001);

	// Joint angles (determined from joint velocities and accelerations and time-step)
	ibex::Interval theta1 = theta1_ + theta1_dot * pred_time + theta1_ddot * sqr(pred_time);
	ibex::Interval theta2 = theta2_ + theta2_dot * pred_time + theta2_ddot * sqr(pred_time);
	ibex::Interval theta3 = theta3_ + theta3_dot * pred_time + theta3_ddot * sqr(pred_time);
	ibex::Interval theta4 = theta4_ + theta4_dot * pred_time + theta4_ddot * sqr(pred_time);
	ibex::Interval theta5 = theta5_ + theta5_dot * pred_time + theta5_ddot * sqr(pred_time);
	ibex::Interval theta6 = theta6_ + theta6_dot * pred_time + theta6_ddot * sqr(pred_time);
	ibex::Interval theta7 = theta7_ + theta7_dot * pred_time + theta7_ddot * sqr(pred_time);

	// Crop to joint angle limits
	theta1 &= ibex::Interval(-2.8973,2.8973);
	theta2 &= ibex::Interval(-1.7628,1.7628);
	theta3 &= ibex::Interval(-2.8973,2.8973);
	theta4 &= ibex::Interval(-3.0718,-0.0698);
	theta5 &= ibex::Interval(-2.8973,2.8973);
	theta6 &= ibex::Interval(-0.0175,3.7525);
	theta7 &= ibex::Interval(-2.8973,2.8973);

	//#######################################
	// Build the kinematic chain of the FRANKA
	//#######################################
	// Create frames (a, alpha, d, theta)
	Frame frame01 = Frame::modifiedDH(0,0,d1,theta1);
	Frame frame12 = Frame::modifiedDH(0,-M_PI/2,0,theta2);
	Frame frame23 = Frame::modifiedDH(0,M_PI/2,d3,theta3);
	Frame frame34 = Frame::modifiedDH(a4,M_PI/2,0,theta4);
	Frame frame45 = Frame::modifiedDH(a5,-M_PI/2,d5,theta5);
	Frame frame56 = Frame::modifiedDH(0,M_PI/2,0,theta6);
	Frame frame67 = Frame::modifiedDH(a7,M_PI/2,0,theta7);
	Frame frame78 = Frame::modifiedDH(0,0,d8,0);

	// Creating additional frames for segment
	Frame frame23A = frame23; frame23A.position[1] += 0.02;
	ibex::IntervalVector p45A(3); p45A[0] = -0.0880; p45A[1] = 0.03; p45A[2] = 0.0;
	Frame frame45A = Frame::Position_and_orientation(p45A, ibex::Matrix::eye(3));

	// Frame products
	Frame frame02 = frame01 * frame12;
	Frame frame03 = frame02 * frame23;
	Frame frame04 = frame03 * frame34;
	Frame frame05 = frame04 * frame45;
	Frame frame06 = frame05 * frame56;
	Frame frame07 = frame06 * frame67;
	Frame frame08 = frame07 * frame78;

	Frame frame03A = frame02 * frame23A;
	Frame frame05A = frame04 * frame45A;

	//#######################################
	// Build a geometric model of the FRANKA
	//#######################################
	ChainGeometry robotChainGeometry;
	ibex::IntervalVector err_RPosition(3,ibex::Interval(-err_RP,err_RP));
	ibex::IntervalVector pBase(3,0);
	pBase += err_RPosition;
	frame01.position += err_RPosition;
	frame02.position += err_RPosition;
	frame03.position += err_RPosition;
	frame04.position += err_RPosition;
	frame05.position += err_RPosition;
	frame06.position += err_RPosition;
	frame07.position += err_RPosition;
	frame08.position += err_RPosition;
	robotChainGeometry.addSegment(SegmentGeometry(pBase, frame02.position, 0.1));
	robotChainGeometry.addSegment(SegmentGeometry(frame02.position, frame03A.position, 0.05));
	robotChainGeometry.addSegment(SegmentGeometry(frame03A.position, frame04.position, 0.05));
	robotChainGeometry.addSegment(SegmentGeometry(frame04.position, frame05A.position, 0.05));
	robotChainGeometry.addSegment(SegmentGeometry(frame05A.position, frame06.position, 0.05));
	robotChainGeometry.addSegment(SegmentGeometry(frame06.position, frame07.position, 0.05));
	robotChainGeometry.addSegment(SegmentGeometry(frame07.position, frame08.position, 0.05));

	//################################################
	// Determine future range of joint positions of the HUMAN
	//################################################
	ibex::Interval err_HPosition = ibex::Interval(-err_HP,err_HP);
	ibex::SystemFactory factory;
	ibex::Variable Psx, Psy, Psz;
	ibex::Variable Pex, Pey, Pez;
	ibex::Variable Pwx, Pwy, Pwz;
	ibex::Variable Phx, Phy, Phz;
	// Add variables
	factory.add_var(Psx);
	factory.add_var(Psy);
	factory.add_var(Psz);
	factory.add_var(Pex);
	factory.add_var(Pey);
	factory.add_var(Pez);
	factory.add_var(Pwx);
	factory.add_var(Pwy);
	factory.add_var(Pwz);
	factory.add_var(Phx);
	factory.add_var(Phy);
	factory.add_var(Phz);
	// Add kinematic constraints
	factory.add_ctr(sqr(Psx - Pex) + sqr(Psy - Pey) + sqr(Psz - Pez) =
			sqr(lu + ibex::Interval(-err_lu,err_lu)));
	factory.add_ctr(sqr(Pwx - Pex) + sqr(Pwy - Pey) + sqr(Pwz - Pez) =
			sqr(lf + ibex::Interval(-err_lu,err_lu)));
	factory.add_ctr(sqr(Pwx - Phx) + sqr(Pwy - Phy) + sqr(Pwz - Phz) =
			sqr(lh + ibex::Interval(-err_lu,err_lu)));
	// Add future prediction
	factory.add_ctr(Psx = Ps[0] + err_HPosition + Ps_dot[0] * pred_time);
	factory.add_ctr(Psy = Ps[1] + err_HPosition + Ps_dot[1] * pred_time);
	factory.add_ctr(Psz = Ps[2] + err_HPosition + Ps_dot[2] * pred_time);
	factory.add_ctr(Pex = Pe[0] + err_HPosition + Pe_dot[0] * pred_time);
	factory.add_ctr(Pey = Pe[1] + err_HPosition + Pe_dot[1] * pred_time);
	factory.add_ctr(Pez = Pe[2] + err_HPosition + Pe_dot[2] * pred_time);
	factory.add_ctr(Pwx = Pw[0] + err_HPosition + Pw_dot[0] * pred_time);
	factory.add_ctr(Pwy = Pw[1] + err_HPosition + Pw_dot[1] * pred_time);
	factory.add_ctr(Pwz = Pw[2] + err_HPosition + Pw_dot[2] * pred_time);
	factory.add_ctr(Phx = Ph[0] + err_HPosition + Ph_dot[0] * pred_time);
	factory.add_ctr(Phy = Ph[1] + err_HPosition + Ph_dot[1] * pred_time);
	factory.add_ctr(Phz = Ph[2] + err_HPosition + Ph_dot[2] * pred_time);
	// Setup contractors
	ibex::System ibexSystem(factory);
	ibex::CtcHC4 hc4(ibexSystem,0.01);
	ibex::CtcHC4 hc4_2(ibexSystem,0.1,true);
	ibex::CtcAcid acid(ibexSystem, hc4_2);
	ibex::CtcCompo compo(hc4,acid);
	// Get initial box
	ibex::IntervalVector jointBox(12,ibex::Interval::ALL_REALS);
	// Apply contractors
	compo.contract(jointBox);

	//#######################################
	// Build a geometric model of the HUMAN
	//#######################################
	ChainGeometry humanChainGeometry;
	ibex::IntervalVector pShoulder(3);
	pShoulder[0] = jointBox[0];
	pShoulder[1] = jointBox[1];
	pShoulder[2] = jointBox[2];
	ibex::IntervalVector pElbow(3);
	pElbow[0] = jointBox[3];
	pElbow[1] = jointBox[4];
	pElbow[2] = jointBox[5];
	ibex::IntervalVector pWrist(3);
	pWrist[0] = jointBox[6];
	pWrist[1] = jointBox[7];
	pWrist[2] = jointBox[8];
	ibex::IntervalVector pHand(3);
	pHand[0] = jointBox[9];
	pHand[1] = jointBox[10];
	pHand[2] = jointBox[11];
	humanChainGeometry.addSegment(SegmentGeometry(pShoulder, pElbow, rad_lu));
	humanChainGeometry.addSegment(SegmentGeometry(pElbow, pWrist, rad_lf));
	humanChainGeometry.addSegment(SegmentGeometry(pWrist, pHand, rad_lh));

	//##################################################################
	// Calculate the minimum non-negative distance between geometries
	//##################################################################
	MinGeometry minGeometry = robotChainGeometry.minDist(humanChainGeometry, allowdist);

	// End time
	auto stop = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
	std::cout << duration.count() << " milliseconds" << std::endl;

	std::cout << "Minimum distance: " << minGeometry.minDist << std::endl;
	std::cout << "Closest segments from first chain: ";
	for (int iseg=0; iseg<minGeometry.nearSegment1.size(); iseg++)
		std::cout << "\t" << minGeometry.nearSegment1[iseg];
	std::cout << std::endl;
	std::cout << "Closest segments from second chain: ";
		for (int iseg=0; iseg<minGeometry.nearSegment2.size(); iseg++)
			std::cout << "\t" << minGeometry.nearSegment2[iseg];
	std::cout << std::endl;

	//##################################################################
	// Exporting results
	//##################################################################
	std::string fileExport("/tmp/predicted_workspace.txt");
	// Clear fileExport
	std::ofstream ofs;
	ofs.open(fileExport, std::ofstream::out | std::ofstream::trunc);

	// Franka and Human number of segments
	ofs <<  robotChainGeometry.Segments.size() << '\t' << humanChainGeometry.Segments.size() << '\n';
	// Min distance
	ofs <<  minGeometry.minDist << '\n';
	// Franka closest segments ID
	for (int iseg=0; iseg<minGeometry.nearSegment1.size(); iseg++)
		ofs << minGeometry.nearSegment1[iseg]+1 << '\t';
	ofs << '\n';
	// Human closest segments ID
	for (int iseg=0; iseg<minGeometry.nearSegment2.size(); iseg++)
		ofs << minGeometry.nearSegment2[iseg]+1 << '\t';
	ofs << '\n';
	// Franka segments end points
	for (int iseg=0; iseg<robotChainGeometry.Segments.size(); iseg++){
		writeBox(ofs, robotChainGeometry.Segments[iseg].start_box());
		writeBox(ofs, robotChainGeometry.Segments[iseg].end_box());
		ofs << '\n';
	}
	// Human segments end points
	for (int iseg=0; iseg<humanChainGeometry.Segments.size(); iseg++){
		writeBox(ofs, humanChainGeometry.Segments[iseg].start_box());
		writeBox(ofs, humanChainGeometry.Segments[iseg].end_box());
		ofs << '\n';
	}
	ofs.close();

	std::cout << "Finished" << std::endl;

}

void writeBox(std::ofstream &os, ibex::IntervalVector box){
	os << box[0].lb() << '\t' << box[0].ub() << '\t' << box[1].lb() << '\t' << box[1].ub() << '\t' << box[2].lb() << '\t' << box[2].ub() << '\t';
}





