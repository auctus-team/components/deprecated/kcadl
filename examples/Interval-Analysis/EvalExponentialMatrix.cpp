/*
 * EvalExponentialMatrix.cpp
 *
 *  Created on: Nov 12, 2018
 *  Author: Joshua Pickard
 */

#include "kcadl.h"

using namespace kcadl;

int main() {
	ibex::IntervalMatrix M_A(3,3);
	M_A[0][0] = -131;
	M_A[0][1] = 19;
	M_A[0][2] = 18;
	M_A[1][0] = -390;
	M_A[1][1] = 56;
	M_A[1][2] = 54;
	M_A[2][0] = -387;
	M_A[2][1] = 57 + ibex::Interval(-0.001,0.001);
	M_A[2][2] = 52;

	std::cout << "Matrix 1:" << std::endl;
	std::cout << M_A << std::endl;
	M_A = 0.1*M_A;

	std::cout << "Horner" << std::endl;
	ibex::IntervalMatrix M_Exp_A = ExponentialMatrices::GetMatrixExponentialHorner(M_A);
	std::cout << M_Exp_A << std::endl;
	std::cout << M_Exp_A.diam() << std::endl;

	std::cout << "Scaled and Squared" << std::endl;
	M_Exp_A = ExponentialMatrices::GetMatrixExponential(M_A);
	std::cout << M_Exp_A << std::endl;
	std::cout << M_Exp_A.diam() << std::endl;

	//------------------------------------------------

	M_A = ibex::IntervalMatrix(2,2);
	M_A[0][0] = 0;
	M_A[0][1] = 1;
	M_A[1][0] = 0;
	M_A[1][1] = ibex::Interval(-3,-2);

	std::cout << "Matrix 2:" << std::endl;
	std::cout << M_A << std::endl;
	M_A = 0.1*M_A;

	std::cout << "Horner" << std::endl;
	M_Exp_A = ExponentialMatrices::GetMatrixExponentialHorner(M_A);
	std::cout << M_Exp_A << std::endl;
	std::cout << M_Exp_A.diam() << std::endl;

	std::cout << "Scaled and Squared" << std::endl;
	M_Exp_A = ExponentialMatrices::GetMatrixExponential(M_A);
	std::cout << M_Exp_A << std::endl;
	std::cout << M_Exp_A.diam() << std::endl;

	std::cout << " Finished" << std::endl;
	return 0;


}

