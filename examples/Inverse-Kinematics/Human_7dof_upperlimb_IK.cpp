/*
 * Human_7dof_upperlimb_IK.cpp
 *
 *  Created on: Dec 3, 2018
 *  Author: Joshua Pickard
 */

#include "kcadl.h"

int main() {
	bool verbose = true;

	ibex::System system("examples/Inverse-Kinematics/minibex/Human_7dof_upperlimb_IK.mbx");
	std::cout << system << std::endl;

	// building contractors
	ibex::CtcHC4 hc4(system,0.01);
	ibex::CtcHC4 hc4_2(system,0.1,true);
	ibex::CtcAcid acid(system, hc4_2);
	ibex::CtcCompo compo(hc4,acid);

	// Create a smear-function bisection heuristic
	ibex::SmearSumRelative bisector(system, 1e-02);

	// Create a "stack of boxes" (CellStack) (depth-first search)
	ibex::CellStack buff;

	// Vector precisions required on variables
	ibex::Vector prec(6, 1e-02);

	// Create a solver
	ibex::Solver s(system, compo, bisector, buff, prec, prec);

	// Run the solver
	s.solve(system.box);
	const ibex::CovList& cov = s.get_data();

	// Display the solutions
	if (verbose) std::cout << "Solutions" << std::endl;
	if (verbose) std::cout << cov.size() << std::endl;
	std::vector<ibex::IntervalVector> solverSolutions;
	for (size_t i=0; i<cov.size(); i++) {
		solverSolutions.push_back(cov[i]);
	}

	for (int sol=0; sol<solverSolutions.size(); sol++) {
		// Structure to save solutions
		std::cout << "#############################" << std::endl;
		std::cout << "Solution: " << sol << std::endl;
		ibex::IntervalVector box = solverSolutions[sol];
		std::cout << box.subvector(0,6) << std::endl;
		for (int i=0;i<7;i++)
			std::cout << "theta" << i+1 << "=" << box[i].mid() << "," << std::endl;

	}

	/* Report performances */
	std::cout << "cpu time used=" << s.get_time() << "s."<< std::endl;
	std::cout << "number of cells=" << s.get_nb_cells() << std::endl;
	return 0;
}



