/*
 * Spatial_IK.cpp
 *
 *  Created on: Nov 23, 2018
 *  Author: Joshua Pickard
 *
 *  TODO: NOT WORKING
 */

#include "kcadl.h"

using namespace kcadl;

int main() {
	// Planar manipulator
	ibex::Interval d2 = 2;
	ibex::Interval a3 = 2;
	ibex::Interval alpha2 = M_PI/2;

	// Create segments
	Segment segmentBase(Joint(Joint::JointType::Base),Frame::Identity());
	Segment segment1(Joint(Joint::JointType::Revolute),Frame::modifiedDH(0,0,0,0));
	Segment segment2(Joint(Joint::JointType::Revolute),Frame::modifiedDH(0,alpha2,d2,-M_PI/2));
	Segment segment3(Joint(Joint::JointType::Revolute),Frame::modifiedDH(a3,0,0,0));

	// Build kinematic chain
	KinematicChain kinematicChain;
	kinematicChain.verbose = false; // Do not display debugging comments
	kinematicChain.addSegment(segmentBase);
	kinematicChain.addSegment(segment1);
	kinematicChain.addSegment(segment2);
	kinematicChain.addSegment(segment3);

	// Desired Poses
	ibex::IntervalMatrix rotationMatrix = ibex::Interval(-1,1)*ibex::IntervalMatrix(ibex::Matrix::ones(3));
	ibex::IntervalVector positionVector(3);
	positionVector[0] = ibex::Interval(2);
	positionVector[1] = ibex::Interval(2);
	positionVector[2] = ibex::Interval(0);

	// Find all IK solutions - position with arbitrary orientation
	std::vector<KinematicChain::KinematicsSolution> solutions = kinematicChain.solveIK(positionVector, rotationMatrix);
	for (int sol=0; sol<solutions.size(); sol++){
		ibex::IntervalVector jointvalue = solutions[sol].jointValues;
		std::cout << "eePosition: " << solutions[sol].eePosition << std::endl;
		std::cout << "eeOrientation: " << solutions[sol].eeOrientation << std::endl;
		for (int jointNum=0; jointNum<jointvalue.size(); jointNum++){
			ibex::Interval theta = jointvalue[jointNum];
			std::cout << "theta: " << jointNum+1 << theta << "rads\t" << theta*180/M_PI << "degs" << std::endl;
		}
	}

	// Visualize results - plot kinematic chain
	vibes::beginDrawing();
	vibes::newFigure("IK Solutions - position with arbitrary orientation");
	vibes::axisLimits(-10,10,-10,10);
	vibes::setFigureProperty("width",600);
	vibes::setFigureProperty("height",600);
	std::vector< double > x_axisx = {0, 1};
	std::vector< double > x_axisy = {0, 0};
	vibes::drawLine (x_axisx, x_axisy, "r");
	std::vector< double > y_axisx = {0, 0};
	std::vector< double > y_axisy = {0, 1};
	vibes::drawLine (y_axisx, y_axisy, "g");
	int x_axis = 0;
	int y_axis = 2;
	for (int index=0; index<solutions.size(); index++){
		std::vector<ibex::IntervalVector> framePositions = solutions[index].framePositions;
		ibex::IntervalVector prev_position = framePositions[0];
		for (int frame=0; frame<framePositions.size(); frame++){
			ibex::IntervalVector position = framePositions[frame];
			// Draw joints as boxes or circles
			if (position.diam().max() > 0.001)
				vibes::drawBox(position[x_axis].lb(), position[x_axis].ub(), position[y_axis].lb(), position[y_axis].ub(), "k[b]");
			else
				vibes::drawEllipse (position[x_axis].mid(), position[y_axis].mid(), 0.001, 0.001, 0.001, "k");
			// Draw lines connecting centers of joints
			std::vector< double > x;
			x.push_back(prev_position[x_axis].mid());
			x.push_back(position[x_axis].mid());
			std::vector< double > y;
			y.push_back(prev_position[y_axis].mid());
			y.push_back(position[y_axis].mid());
			vibes::drawLine (x, y, "k");
			// Update previous
			prev_position = position;
		}
	}

	std::cout << " Finished" << std::endl;
	return 0;

}

