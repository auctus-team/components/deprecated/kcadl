include(FindPackageHandleStandardArgs)

find_path(VIBES_INCLUDE_DIR
          NAMES vibes.h
          HINTS "/opt/kcadl/3rd/vibes/client-api/C++/src"
                ENV VIBES_SOURCE_DIR
          PATH_SUFFIXES include)

                   
set(VIBES_INCLUDE_DIRS ${VIBES_INCLUDE_DIR})

find_package_handle_standard_args(VIBES DEFAULT_MSG VIBES_INCLUDE_DIRS)
set(VIBES_FOUND ${VIBES_FOUND})

