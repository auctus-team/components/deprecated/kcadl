set (KCADL_PKGCONFIG_LINK_FLAGS)
set (KCADL_PKGCONFIG_LIBS)
set (KCADL_PKGCONFIG_LINK_DIRS)
set (KCADL_PKGCONFIG_INCDIRS)

function (pkgconfig_preprocess_property VAR TARGET PROP)
  get_target_property (OUT_PROP ${TARGET} ${PROP})
  string(REPLACE "$<BUILD_INTERFACE:" "$<0:" OUT_PROP "${OUT_PROP}")
  string(REPLACE "$<INSTALL_INTERFACE:" "$<1:" OUT_PROP "${OUT_PROP}")

  string(REPLACE "$<INSTALL_PREFIX>/${CMAKE_INSTALL_INCLUDEDIR}" "\${includedir}" OUT_PROP "${OUT_PROP}")
  string(REPLACE "$<INSTALL_PREFIX>/${CMAKE_INSTALL_LIBDIR}" "\${libdir}" OUT_PROP "${OUT_PROP}")
  string(REPLACE "$<INSTALL_PREFIX>" "\${prefix}" OUT_PROP "${OUT_PROP}")

  set(${VAR} ${OUT_PROP} PARENT_SCOPE)
endfunction ()

# -I, -L and -l
pkgconfig_preprocess_property (incdirs kcadl INTERFACE_INCLUDE_DIRECTORIES)
list (APPEND Libs "-L\${libdir} -L/opt/kcadl/3rd/ibex-lib/build/lib -L/opt/kcadl/3rd/ibex-lib/build/lib/ibex/3rd -L/opt/kcadl/3rd/CGAL-4.13.1/build/lib -L/opt/kcadl/3rd/qhull/lib")
list (APPEND Libs "-lkcadl -lboost_thread -lm -libex -lprim -lgmp -lmpfr -lsoplex -lz -lCGAL -lqhullcpp -lqhullstatic_r")
list (APPEND Cflags "-I/opt/kcadl/3rd/CGAL-4.13.1/build/include -I/opt/kcadl/3rd/qhull/src")

file (GENERATE OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/kcadl.pc
               CONTENT "prefix=${CMAKE_INSTALL_PREFIX}
includedir=\${prefix}/${CMAKE_INSTALL_INCLUDEDIR}
libdir=\${prefix}/${CMAKE_INSTALL_LIBDIR}

Name: KCADL
Description: ${KCADL_DESCRIPTION}
Url: ${KCADL_URL}
Version: ${KCADL_VERSION}
Cflags: -I$<JOIN:${incdirs}, -I>
Libs: $<JOIN:${Libs}, >
")
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/kcadl.pc DESTINATION ${CMAKE_INSTALL_PKGCONFIG})
