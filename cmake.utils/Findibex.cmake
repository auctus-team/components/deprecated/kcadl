include(FindPackageHandleStandardArgs)

find_path(ibex_INCLUDE_DIR
          NAMES ibex.h
          HINTS "/opt/kcadl/3rd/ibex-lib/build/include"
                ENV ibex_SOURCE_DIR
          PATH_SUFFIXES include)

find_path(ibex_LIB_DIR
             NAMES libibex.a
             HINTS "/opt/kcadl/3rd/ibex-lib/build/lib"
                   ENV ibex_SOURCE_DIR
             PATH_SUFFIXES lib
                           libs)

find_library(ibex_LIBRARY 
              NAMES ibex
              PATHS "/opt/kcadl/3rd/ibex-lib/build/lib"
                   ENV ibex_SOURCE_DIR
                   )

find_library(prim_LIBRARY  
                NAMES prim  
                HINTS "/opt/kcadl/3rd/ibex-lib/build/lib/ibex/3rd" 
                ENV ibex_SOURCE_DIR
)

find_library(soplex_LIBRARY  
                NAMES soplex
                HINTS "/opt/kcadl/3rd/ibex-lib/build/lib/ibex/3rd" 
                ENV ibex_SOURCE_DIR
)

find_library(z_LIBRARY  
                NAMES z
)
                   
set(ibex_INCLUDE_DIRS ${ibex_INCLUDE_DIR} ${ibex_INCLUDE_DIR}/ibex ${ibex_INCLUDE_DIR}/ibex/3rd)
set(ibex_LIB_DIRS ${ibex_LIB_DIR} ${ibex_LIB_DIR}/ibex/3rd)
set(ibex_LIBRARIES ${ibex_LIBRARY} ${mpfr_LIBRARY} ${gmp_LIBRARY} ${ibex_LIBRARY} ${prim_LIBRARY} ${soplex_LIBRARY} ${z_LIBRARY} )

find_package_handle_standard_args(ibex DEFAULT_MSG ibex_LIB_DIRS
                                                   ibex_LIBRARIES
                                                   ibex_INCLUDE_DIRS)
set(ibex_FOUND ${ibex_FOUND})

