/*
 * This software is the property of Auctus, the INRIA research team.
 * Please refer to the Licence file of the repository and Authors
 */

/**
 * @defgroup ExponentialMatrices ExponentialMatrices
 *
 * @brief Class for generating an exponential interval matrix.
 *
 * @author Joshua Pickard
 * Contact: joshua.pickard@inria.fr
 *
 */

#ifndef SRC_EXPONENTIALMATRICES_H_
#define SRC_EXPONENTIALMATRICES_H_

#include "ibex.h"
#include "Twist.h"

namespace kcadl{
class ExponentialMatrices {
public:
	/**
	ExponentialMatrices constructor
	*/
	ExponentialMatrices();

	/**
	ExponentialMatrices destructor
	*/
	virtual ~ExponentialMatrices();

	/**
	Return the matrix exponential from an interval matrix
	\param A interval matrix
	\return interval matrix exponential
	*/
	static ibex::IntervalMatrix GetMatrixExponential(const ibex::IntervalMatrix &A);

	/**
	Return the matrix exponential from an interval matrix using the Horner scheme
	\param A interval matrix
	\return interval matrix exponential
	*/
	static ibex::IntervalMatrix GetMatrixExponentialHorner(const ibex::IntervalMatrix &A);

	/**
	Compute the matrix exponential from the twist vector and joint value.
	\param twist twist vector
	\param theta theta interval
	\return interval matrix exponential
	*/
	static ibex::IntervalMatrix MatrixExponential(Twist &twist, ibex::Interval theta);

private:

	/**
	Computes the matrix exponential for the interval matrix A using the scaling and squaring process from:
	Goldsztejn A. Neumaier A., On the Exponentiation of Interval Matrices, Reliable Computing, 2014, volume 20,53-72
	\param A interval matrix
	\return interval matrix exponential
	*/
	static ibex::IntervalMatrix MatrixExponential(const ibex::IntervalMatrix &A);

	/**
	Computes the matrix exponential for the interval matrix A using the horner scheme from:
	Goldsztejn A. Neumaier A., On the Exponentiation of Interval Matrices, Reliable Computing, 2014, volume 20,53-72
	\param A interval matrix
	\return interval matrix exponential
	*/
	static ibex::IntervalMatrix MatrixExponentialHorner(const ibex::IntervalMatrix &A);

	/**
	Computes the Horner evaluation of an interval matrix A using the method from:
	Goldsztejn A. Neumaier A., On the Exponentiation of Interval Matrices, Reliable Computing, 2014, volume 20,53-72
	\param A interval matrix
	\param K k
	\return Horner evaluation of interval matrix
	*/
	static ibex::IntervalMatrix HornerScheme(const ibex::IntervalMatrix &A, int K);

	static ibex::Interval factorial(ibex::Interval n) {
		if (n.mid() > 1)
			return n * factorial(n - 1);
		else
			return 1;
	}
};


inline ibex::IntervalMatrix ExponentialMatrices::MatrixExponential(Twist &twist, ibex::Interval q){

	ibex::IntervalMatrix expMatrix(4,4);

	// Check if twist is infinite pitch
	if (abs(twist.w.x+twist.w.y+twist.w.z).ub()<0.00001){ // infinite pitch twist
		expMatrix[0][0] = 1;
		expMatrix[0][1] = 0;
		expMatrix[0][2] = 0;
		expMatrix[1][0] = 0;
		expMatrix[1][1] = 1;
		expMatrix[1][2] = 0;
		expMatrix[2][0] = 0;
		expMatrix[2][1] = 0;
		expMatrix[2][2] = 1;
		expMatrix[0][3] = twist.v.x*q;
		expMatrix[1][3] = twist.v.y*q;
		expMatrix[2][3] = twist.v.z*q;
		expMatrix[3][0] = 0;
		expMatrix[3][1] = 0;
		expMatrix[3][2] = 0;
		expMatrix[3][3] = 1;
	} else { // non infinite pitch twist
		expMatrix[0][0] = 1+(1-cos(q))*(-sqr(twist.w.y)-sqr(twist.w.z));
		expMatrix[0][1] = -sin(q)*twist.w.z+(1-cos(q))*twist.w.y*twist.w.x;
		expMatrix[0][2] =  sin(q)*twist.w.y+(1-cos(q))*twist.w.z*twist.w.x;
		expMatrix[1][0] = sin(q)*twist.w.z+(1-cos(q))*twist.w.y*twist.w.x;
		expMatrix[1][1] = 1+(1-cos(q))*(-sqr(twist.w.x)-sqr(twist.w.z));
		expMatrix[1][2] = -sin(q)*twist.w.x+(1-cos(q))*twist.w.z*twist.w.y;
		expMatrix[2][0] = -sin(q)*twist.w.y+(1-cos(q))*twist.w.z*twist.w.x;
		expMatrix[2][1] = sin(q)*twist.w.x+(1-cos(q))*twist.w.z*twist.w.y;
		expMatrix[2][2] = 1+(1-cos(q))*(-sqr(twist.w.x)-sqr(twist.w.y));
		expMatrix[0][3] = (twist.v.y*twist.w.z-twist.v.z*twist.w.y)*(cos(q)-1)+twist.v.x*sin(q);
		expMatrix[1][3] = -(twist.v.x*twist.w.z-twist.v.z*twist.w.x)*(cos(q)-1)+twist.v.y*sin(q);
		expMatrix[2][3] = (twist.v.x*twist.w.y-twist.v.y*twist.w.x)*(cos(q)-1)+twist.v.z*sin(q);
//		expMatrix[0][3] =  (twist.v.y * twist.w.z - twist.v.z * twist.w.y) * cos(q) + (twist.v.x * sqr(twist.w.y) + twist.v.x * sqr(twist.w.z) - twist.v.y * twist.w.x * twist.w.y - twist.v.z * twist.w.x * twist.w.z) * sin(q) + twist.v.z * pow(twist.w.y, 0.3e1) - twist.v.y * sqr(twist.w.y) * twist.w.z + twist.v.z * (sqr(twist.w.x) + sqr(twist.w.z)) * twist.w.y + sqr(twist.w.x) * q * twist.v.x - twist.v.y * sqr(twist.w.x) * twist.w.z - twist.v.y * pow(twist.w.z, 0.3e1);
//		expMatrix[1][3] =  -(twist.v.x * twist.w.z - twist.v.z * twist.w.x) * cos(q) + (-twist.v.x * twist.w.x * twist.w.y + twist.v.y * sqr(twist.w.x) + twist.v.y * sqr(twist.w.z) - twist.v.z * twist.w.y * twist.w.z) * sin(q) - twist.v.z * pow(twist.w.x, 0.3e1) + twist.v.x * sqr(twist.w.x) * twist.w.z - twist.v.z * (sqr(twist.w.y) + sqr(twist.w.z)) * twist.w.x + twist.v.x * sqr(twist.w.y) * twist.w.z + twist.v.x * pow(twist.w.z, 0.3e1) + sqr(twist.w.y) * q * twist.v.y;
//		expMatrix[2][3] =  (twist.v.x * twist.w.y - twist.v.y * twist.w.x) * cos(q) + (-twist.v.x * twist.w.x * twist.w.z - twist.v.y * twist.w.y * twist.w.z + twist.v.z * sqr(twist.w.x) + twist.v.z * sqr(twist.w.y)) * sin(q) + twist.v.y * pow(twist.w.x, 0.3e1) - twist.v.x * sqr(twist.w.x) * twist.w.y + twist.v.y * (sqr(twist.w.y) + sqr(twist.w.z)) * twist.w.x - twist.v.x * pow(twist.w.y, 0.3e1) - twist.v.x * twist.w.y * sqr(twist.w.z) + sqr(twist.w.z) * q * twist.v.z;

		expMatrix[3][0] = 0;
		expMatrix[3][1] = 0;
		expMatrix[3][2] = 0;
		expMatrix[3][3] = 1;
	}

	return expMatrix;

}

inline ibex::IntervalMatrix ExponentialMatrices::GetMatrixExponential(const ibex::IntervalMatrix &A){
	return ExponentialMatrices::MatrixExponential(A);
}


inline ibex::IntervalMatrix ExponentialMatrices::GetMatrixExponentialHorner(const ibex::IntervalMatrix &A){
	return ExponentialMatrices::MatrixExponentialHorner(A);
}

inline ibex::IntervalMatrix ExponentialMatrices::MatrixExponential(const ibex::IntervalMatrix &A){
	double infNorm = infinite_norm(A);
	double K = 100;//ceil(infNorm + 2);
	double L = 5;//ceil(log(infNorm/(K+2))/log(2));


	// Check if (K+2)*2^L > infinite_norm(A) criteria is satisfied
	if ((K+2)*pow(2,L) <= infinite_norm(A))
		std::cout << "K,L criteria NOT satisfied" <<  std::endl;

	ibex::Interval powers = 2;
	for (int i=1; i<L; i++) powers *= 2;
	ibex::Interval scaling = 1/powers;
	ibex::IntervalMatrix M_Scaled = scaling*A;
	ibex::IntervalMatrix M_ExpScaled = HornerScheme(M_Scaled, K);
	ibex::IntervalMatrix M_Exp = M_ExpScaled;
	for (int i=1; i<pow(2,L); i++){
		M_Exp *= M_ExpScaled;
	}

	return M_Exp;
}

inline ibex::IntervalMatrix ExponentialMatrices::MatrixExponentialHorner(const ibex::IntervalMatrix &A){
	double infNorm = infinite_norm(A);
	double K = 100;//ceil(infNorm + 2);

	// Check if (K+2) > infinite_norm(A) criteria is satisfied
	if ((K+2) <= infinite_norm(A))
		 std::cout << "K criteria NOT satisfied" <<  std::endl;

	ibex::IntervalMatrix M_Exp = HornerScheme(A, K);

	return M_Exp;
}

inline ibex::IntervalMatrix ExponentialMatrices::HornerScheme(const ibex::IntervalMatrix &A, int K){
	double alpha = infinite_norm(A);
	ibex::Matrix Eye = ibex::Matrix::eye(A.nb_cols());

	ibex::IntervalMatrix M_HornerApprox(A.nb_rows(),A.nb_cols());
	for (int k=K; k>=1; k--){
		if (k==K){
			M_HornerApprox = Eye + (1.0/k) * A;
		} else{
			M_HornerApprox = Eye + (1.0/k) * A * M_HornerApprox;
		}
	}

    // alpha is equal to the infinity norm
	ibex::Interval num = alpha; // make use of ibex::Interval to ensure correct representation of large numbers
	for (int i=1; i<K+1; i++) num *= alpha;
	ibex::Interval den = (factorial(ibex::Interval(K+1))) * (1- (alpha/(K+2))); // make use of ibex::Interval to ensure correct representation of large numbers
	ibex::Interval rho_alpha_K = num / den;
	ibex::IntervalMatrix M_Remainder = ibex::Matrix::zeros(A.nb_cols());
	M_Remainder.inflate(1);
	M_Remainder *= rho_alpha_K;

	return M_HornerApprox + M_Remainder;
}
}
#endif /* SRC_EXPONENTIALMATRICES_H_ */
