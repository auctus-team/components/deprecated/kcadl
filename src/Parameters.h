/*
 * This software is the property of Auctus, the INRIA research team.
 * Please refer to the Licence file of the repository and Authors
 */

/**
 * @defgroup Parameters Parameters
 *
 * @brief Class for storing parameters.
 *
 * @author Joshua Pickard
 * Contact: joshua.pickard@inria.fr
 *
 */

#ifndef PARAMETERS_H_
#define PARAMETERS_H_

#include "ibex.h"

namespace kcadl{
class Parameters {
public:
	Parameters();
	virtual ~Parameters();

	ibex::IntervalVector outer = ibex::IntervalVector::empty(1);
	ibex::IntervalVector inner = ibex::IntervalVector::empty(1);
};
}
#endif /* PARAMETERS_H_ */
