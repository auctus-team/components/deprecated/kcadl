/*
 * This software is the property of Auctus, the INRIA research team.
 * Please refer to the Licence file of the repository and Authors
 */

/**
 * @defgroup Joint Joint
 *
 * @brief Class for joint types.
 *
 * @author Joshua Pickard
 * Contact: joshua.pickard@inria.fr
 *
 */

#ifndef SRC_JOINT_H_
#define SRC_JOINT_H_

namespace kcadl{
class Joint {
public:
	typedef enum {Base, EE, Revolute, Prismatic, None} JointType; /**< Available joint type */

	/**
	Joint constructor
	 */
	Joint();

	/**
	Joint constructor
	\param type joint type
	 */
	Joint(JointType type);

	/**
	Joint destructor
	\param type joint type
	 */
	virtual ~Joint();


private:
	Joint::JointType type; /**< Type of joint */
};
}
#endif /* SRC_JOINT_H_ */
