/*
 * This software is the property of Auctus, the INRIA research team.
 * Please refer to the Licence file of the repository and Authors
 */

/**
 * @file Twist.cpp
 *
 * @brief Class for twists.
 *
 * @ingroup Twist
 *
 * @author Joshua Pickard
 * Contact: joshua.pickard@inria.fr
 *
 */

#include "Twist.h"
#include "ibex.h"

namespace kcadl{
Twist::Twist(){
}

Twist::Twist(ibex::IntervalVector &V, ibex::IntervalVector &W){
	v.x = V[0];
	v.y = V[1];
	v.z = V[2];

	ibex::Interval normW = W[0] * W[0] + W[1] * W[1] + W[2] * W[2];
	// Case of infinite pitch
	if (normW.ub()<0.0001)
		normW = 1;

	w.x = 1/(sqrt(normW)) * W[0];
	w.y = 1/(sqrt(normW)) * W[1];
	w.z = 1/(sqrt(normW)) * W[2];

	twist[0] = v.x;
	twist[1] = v.y;
	twist[2] = v.z;
	twist[3] = w.x;
	twist[4] = w.y;
	twist[5] = w.z;

	GetRotationW();
}
Twist::Twist(ibex::Vector &V, ibex::Vector &W) {
	v.x = ibex::Interval(V[0]);
	v.y = ibex::Interval(V[1]);
	v.z = ibex::Interval(V[2]);

	ibex::Interval normW = W[0] * W[0] + W[1] * W[1] + W[2] * W[2];
	// Case of infinite pitch
	if (normW.ub()<0.0001)
		normW = 1;

	w.x = 1/(sqrt(normW)) * ibex::Interval(W[0]);
	w.y = 1/(sqrt(normW)) * ibex::Interval(W[1]);
	w.z = 1/(sqrt(normW)) * ibex::Interval(W[2]);


	twist[0] = v.x;
	twist[1] = v.y;
	twist[2] = v.z;
	twist[3] = w.x;
	twist[4] = w.y;
	twist[5] = w.z;

	GetRotationW();
}
Twist::Twist(double Vx, double Vy, double Vz, double Wx, double Wy, double Wz){
	v.x = ibex::Interval(Vx);
	v.y = ibex::Interval(Vy);
	v.z = ibex::Interval(Vz);

	ibex::Interval normW = Wx * Wx + Wy * Wy + Wz * Wz;
	// Case of infinite pitch
	if (normW.ub()<0.0001)
		normW = 1;

	w.x = 1/(sqrt(normW)) * ibex::Interval(Wx);
	w.y = 1/(sqrt(normW)) * ibex::Interval(Wy);
	w.z = 1/(sqrt(normW)) * ibex::Interval(Wz);

	twist[0] = v.x;
	twist[1] = v.y;
	twist[2] = v.z;
	twist[3] = w.x;
	twist[4] = w.y;
	twist[5] = w.z;

	GetRotationW();
}
Twist::Twist(ibex::Interval Vx, ibex::Interval Vy, ibex::Interval Vz, ibex::Interval Wx, ibex::Interval Wy, ibex::Interval Wz){
	v.x = Vx;
	v.y = Vy;
	v.z = Vz;

	ibex::Interval normW = Wx * Wx + Wy * Wy + Wz * Wz;
	// Case of infinite pitch
	if (normW.ub()<0.0001)
		normW = 1;

	w.x = 1/(sqrt(normW)) * Wx;
	w.y = 1/(sqrt(normW)) * Wy;
	w.z = 1/(sqrt(normW)) * Wz;

	twist[0] = v.x;
	twist[1] = v.y;
	twist[2] = v.z;
	twist[3] = w.x;
	twist[4] = w.y;
	twist[5] = w.z;

	GetRotationW();
}
Twist::Twist(ibex::Function &varTwist, ibex::IntervalVector &allVarsVector){
//	twistExp = varTwist;
	twist = varTwist.eval_vector(allVarsVector);

	v.x = ibex::Interval(twist[0]);
	v.y = ibex::Interval(twist[1]);
	v.z = ibex::Interval(twist[2]);

	ibex::Interval normW = twist[3] * twist[3] + twist[4] * twist[4] + twist[5] * twist[5];
	// Case of infinite pitch
	if (normW.ub()<0.0001)
		normW = 1;

	w.x = 1/(sqrt(normW)) * ibex::Interval(twist[3]);
	w.y = 1/(sqrt(normW)) * ibex::Interval(twist[4]);
	w.z = 1/(sqrt(normW)) * ibex::Interval(twist[5]);
	twist[3] = w.x;
	twist[4] = w.y;
	twist[5] = w.z;

	GetRotationW();
}

Twist::~Twist() {
	// TODO Auto-generated destructor stub
}

void Twist::GetRotationW(){
	rotationW[0][0] = 0;
	rotationW[0][1] = -w.z;
	rotationW[0][2] = w.y;
	rotationW[1][0] = w.z;
	rotationW[1][1] = 0;
	rotationW[1][2] = -w.x;
	rotationW[2][0] = -w.y;
	rotationW[2][1] = w.x;
	rotationW[2][2] = 0;
}

ibex::IntervalVector Twist::GetV(){
	return ibex::IntervalVector(twist.subvector(0,2));
}


ibex::IntervalVector Twist::GetW(){
	return ibex::IntervalVector(twist.subvector(3,5));
}
}


