/*
 * This software is the property of Auctus, the INRIA research team.
 * Please refer to the Licence file of the repository and Authors
 */

/**
 * @file Frame.cpp
 *
 * @brief Class for coordinate frames.
 *
 * @ingroup Frame
 *
 * @author Joshua Pickard
 * Contact: joshua.pickard@inria.fr
 *
 */

#include "Frame.h"
#include "ibex.h"
#include <assert.h>
#include "Utilities.h"

namespace kcadl{
Frame::~Frame() {
	// TODO Auto-generated destructor stub
}

Frame Frame::Transpose() {
	Frame newFrame;

	newFrame.orientation = orientation.transpose();
	newFrame.position = -orientation.transpose()*position;

	int tempFrame = describedFrame;
	newFrame.describedFrame = referenceFrame;
	newFrame.referenceFrame = tempFrame;

	return newFrame;
}

ibex::IntervalMatrix Frame::TransformationMatrix(){
	return TransformationMatrix(position, orientation);
}

ibex::IntervalMatrix Frame::TransformationMatrix(ibex::IntervalVector &framePosition, ibex::IntervalMatrix &frameOrientation){
	ibex::IntervalMatrix transform(4,4);
	transform[0][0] = frameOrientation[0][0];
	transform[0][1] = frameOrientation[0][1];
	transform[0][2] = frameOrientation[0][2];
	transform[1][0] = frameOrientation[1][0];
	transform[1][1] = frameOrientation[1][1];
	transform[1][2] = frameOrientation[1][2];
	transform[2][0] = frameOrientation[2][0];
	transform[2][1] = frameOrientation[2][1];
	transform[2][2] = frameOrientation[2][2];
	transform[0][3] = framePosition[0];
	transform[1][3] = framePosition[1];
	transform[2][3] = framePosition[2];
	transform[3][0] = 0;
	transform[3][1] = 0;
	transform[3][2] = 0;
	transform[3][3] = 1;
	return transform;
}

Frame Frame::FrameMultiply(Frame &frame, bool use_constraints){
	Frame newFrame;

	// Minibex system
	ibex::System system("minibex/Rotation_matrix.mbx");
	ibex::CtcHC4 hc4(system,0.0001);
	ibex::CtcHC4 hc4_2(system,0.001,true);
	ibex::CtcAcid acid(system, hc4_2);
	ibex::CtcCompo compo(hc4,acid);
	ibex::IntervalVector system_box(9);

	// Check that reference frames and description frames are correct.
	assert(describedFrame==frame.referenceFrame);
	assert((DH_type==-1)||(frame.DH_type==-1)||(DH_type==frame.DH_type));

	// Compute new position and orientation
	newFrame.position = orientation * frame.position + position;
	newFrame.orientation = orientation * frame.orientation;

	if (use_constraints){
		system_box = flatten(newFrame.orientation);
		compo.contract(system_box);
		compo.contract(system_box);
		newFrame.orientation = unflatten(system_box,3,3);
	}

	// Reduce overestimation in rotation matrix
	newFrame.orientation &= ibex::IntervalMatrix(3,3,ibex::Interval(-1,1));

	// Set new describedFrame and referenceFrame
	newFrame.referenceFrame = referenceFrame;
	newFrame.describedFrame = frame.describedFrame;

	// Update associated twist
	ibex::IntervalVector W = newFrame.orientation.col(2); // twist axis
	ibex::IntervalVector V = ibex::cross(-W, newFrame.position); // position of twist axis
	newFrame.twist = Twist(V, W);

	return newFrame;
}

ibex::IntervalMatrix Frame::ZeroTransform(){
	// Compute transform from the DH parameters with theta=0
	ibex::IntervalMatrix transform(4,4);
	transform[0][0] = cos(DH_theta);
	transform[0][1] = -sin(DH_theta)*cos(DH_alpha);
	transform[0][2] = sin(DH_theta)*sin(DH_alpha);
	transform[1][0] = sin(DH_theta);
	transform[1][1] = cos(DH_theta)*cos(DH_alpha);
	transform[1][2] = -cos(DH_theta)*sin(DH_alpha);
	transform[2][0] = 0;
	transform[2][1] = sin(DH_alpha);
	transform[2][2] = cos(DH_alpha);
	transform[0][3] = DH_a*cos(DH_theta);
	transform[1][3] = DH_a*sin(DH_theta);
	transform[2][3] = DH_d;
	transform[3][0] = 0;
	transform[3][1] = 0;
	transform[3][2] = 0;
	transform[3][3] = 1;
	return transform;
}

ibex::IntervalMatrix Frame::InverseZeroTransform(){
	// Compute transform from the DH parameters with theta=0
	ibex::IntervalMatrix orientation(3,3);
	orientation[0][0] = cos(DH_theta);
	orientation[0][1] = -sin(DH_theta)*cos(DH_alpha);
	orientation[0][2] = sin(DH_theta)*sin(DH_alpha);
	orientation[1][0] = sin(DH_theta);
	orientation[1][1] = cos(DH_theta)*cos(DH_alpha);
	orientation[1][2] = -cos(DH_theta)*sin(DH_alpha);
	orientation[2][0] = 0;
	orientation[2][1] = sin(DH_alpha);
	orientation[2][2] = cos(DH_alpha);

	ibex::IntervalVector position(3);
	position[0] = DH_a*cos(DH_theta);
	position[1] = DH_a*sin(DH_theta);
	position[2] = DH_d;

	// Compute inverse
	orientation = orientation.transpose();
	position = -orientation.transpose()*position;
	return TransformationMatrix(position, orientation);
}

void Frame::Update_DH_d(ibex::Interval new_DH_d){
	DH_d = new_DH_d;
	Update_DH();
}

void Frame::Update_DH_theta(ibex::Interval new_DH_theta){
	DH_theta = new_DH_theta;
	Update_DH();
}

void Frame::Update_DH(){
	Frame frame;
	if (DH_type==0)  // classical DH
		frame = DH(DH_a, DH_alpha, DH_d, DH_theta);
	else if (DH_type==1)  // modified DH
		frame = modifiedDH(DH_a, DH_alpha, DH_d, DH_theta);
	position = frame.position;
	orientation = frame.orientation;
}

}
