/*
 * This software is the property of Auctus, the INRIA research team.
 * Please refer to the Licence file of the repository and Authors
 */

/**
 * @file Joint.cpp
 *
 * @brief Class for joint types.
 *
 * @ingroup Joint
 *
 * @author Joshua Pickard
 * Contact: joshua.pickard@inria.fr
 *
 */

#include "Joint.h"

namespace kcadl{
Joint::Joint(){

}

Joint::Joint(JointType jointType) {
	type = jointType;
}

Joint::~Joint() {
	// TODO Auto-generated destructor stub
}
}
