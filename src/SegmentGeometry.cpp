/*
 * This software is the property of Auctus, the INRIA research team.
 * Please refer to the Licence file of the repository and Authors
 */

/**
 * @file SegmentGeometry.cpp
 *
 * @brief Class for generating geometric segments for building imprecise serial kinematic chains.
 *
 * @ingroup SegmentGeometry
 *
 * @author Joshua Pickard
 * Contact: joshua.pickard@inria.fr
 *
 */

#include "SegmentGeometry.h"
#include <assert.h>
#include "ibex.h"

namespace kcadl{

SegmentGeometry::SegmentGeometry(ibex::IntervalVector &startBox, ibex::IntervalVector &endBox, double cylinderRadius) {

	// Get set of points associated with the segment
	VertexPoints(startBox, pointList);
	VertexPoints(endBox, pointList);

	// Store parameters
	sBox = startBox;
	eBox = endBox;
	radius = cylinderRadius;
}

SegmentGeometry::~SegmentGeometry() {
	// TODO Auto-generated destructor stub
}

void SegmentGeometry::VertexPoints(ibex::IntervalVector &box, std::vector<Point_3> &list){
	assert(box.size()==3);

	double boxX;
	double boxY;
	double boxZ;
	for (int x=1; x<=2; x++){
		if (x==1) boxX = box[0].lb();
		else boxX = box[0].ub();
		for (int y=1; y<=2; y++){
			if (y==1) boxY = box[1].lb();
			else boxY = box[1].ub();
			for (int z=1; z<=2; z++){
				if (z==1) boxZ = box[2].lb();
				else boxZ = box[2].ub();
				// Add point to list
				list.push_back(Point_3(boxX, boxY, boxZ));
			}
		}
	}
}

double SegmentGeometry::minDist(const SegmentGeometry &segmentGeometry){
	Polytope_distance pd(pointList.begin(), pointList.end(), segmentGeometry.pointList.begin(), segmentGeometry.pointList.end());
	assert (pd.is_valid());

	// get squared distance
	double dist = sqrt(CGAL::to_double (pd.squared_distance_numerator()) /
		    CGAL::to_double (pd.squared_distance_denominator()));
	dist = std::max(0.0, dist - radius - segmentGeometry.radius);

	return dist;
}

ibex::IntervalVector SegmentGeometry::start_box(){
	return sBox;
}

ibex::IntervalVector SegmentGeometry::end_box(){
	return eBox;
}

} // namespace kcadl

