/*
 * This software is the property of Auctus, the INRIA research team.
 * Please refer to the Licence file of the repository and Authors
 */

/**
 * @defgroup KinematicChain KinematicChain
 *
 * @brief Class for generating imprecise serial kinematic chains from Segment objects.
 *
 * @author Joshua Pickard
 * Contact: joshua.pickard@inria.fr
 *
 */

#ifndef SRC_KINEMATICCHAIN_H_
#define SRC_KINEMATICCHAIN_H_

#ifdef __GNUC__
#define DEPRECATED(func) func __attribute__ ((deprecated))
#elif defined(_MSC_VER)
#define DEPRECATED(func) __declspec(deprecated) func
#else
#pragma message("WARNING: You need to implement DEPRECATED for this compiler")
#define DEPRECATED(func) func
#endif

#include "ibex.h"
#include "Twist.h"
#include "Segment.h"

namespace kcadl{
class KinematicChain {

	struct ChainStruct {
		int elementID; // base = 0
		int chainID; // base = 0
		Twist twist;
		ibex::Interval theta;
		ibex::IntervalMatrix matrixExponential = ibex::IntervalMatrix(4, 4);
	};/**< Chain structure */

public:

	struct KinematicsSolution {
		ibex::IntervalVector jointValues = ibex::IntervalVector::empty(1);
		ibex::IntervalVector eePosition = ibex::IntervalVector::empty(3);
		ibex::IntervalMatrix eeOrientation = ibex::IntervalMatrix::empty(3,3);
		std::vector<ibex::IntervalVector> framePositions;
	}; /**< Kinematics solution */

	/**
	KinematicChain constructor
	 */
	KinematicChain();

	/**
	KinematicChain destructor
	 */
	virtual ~KinematicChain();

	bool verbose = false; /**< Debugging flag */

	/**
	Add a segment to an existing kinematic chain
	\param segment segment to be added to chain
	 */
	void addSegment(const Segment &segment);

	/**
	Generate the zero transform of the kinematic chain
	\param zeroPosition interval vector describing the zero position (end-effector position at joint values of 0)
	\param zeroOrientation interval matrix describing the zero orientation (end-effector orientation at joint values of 0)
	 */
	void zeroTransform(const ibex::IntervalVector &zeroPosition, const  ibex::IntervalMatrix &zeroOrientation);

	/**
	Solve the inverse kinematics of the kinematic chain
	\param positionEE interval vector describing the end-effector position
	\param positionEE interval matrix describing the end-effector orientation
	\return a list of kinematic solutions
	 */
	std::vector<KinematicsSolution> solveIK(const ibex::IntervalVector &positionEE, const  ibex::IntervalMatrix &orientationEE);

	/**
	Solve the forward kinematics for a given set of joint values.
	\param jointValues interval vector of joint values
	\param use_filtering apply filtering to equations
	\return a list of kinematics solutions
	 */
	std::vector<KinematicsSolution> solveFK(const ibex::IntervalVector &jointValues, bool use_filtering=false, bool base_reference=true);

	// Workspace solvers
	//std::vector<ibex::IntervalVector> reachableWorkspace(ibex::IntervalVector &searchBox, double resolution);

private:
	// Defining multiple kinematic chains
	std::vector<std::vector<ChainStruct>> kinematicChains; /**< List of lists of chain structures describing kinematic chains */
	std::vector<Segment> segments; /**< List of segments describing kinematic chains*/

	// Zero transform
	ibex::IntervalMatrix _zeroOrientation = ibex::IntervalMatrix::empty(3,3); /**< Interval matrix of orientation of zero transform*/
	ibex::IntervalVector _zeroPosition = ibex::IntervalVector::empty(3); /**< Interval vector of position of zero transform*/

	/**
	Generate minibex file
	\param positionEE interval vector describing the end-effector position
	\param positionEE interval matrix describing the end-effector orientationEE
	\return a string description of the minibex file
	 */
	std::string generateMinibex_Basic(const ibex::IntervalVector &positionEE, const  ibex::IntervalMatrix &orientationEE);

	/**
	Compute the joint angles of a kinematic solution
	\param solverSolutions a list of kinematic solution boxes
	\return a list of kinematic solutions
	 */
	std::vector<KinematicsSolution> computeJointAngles(std::vector<ibex::IntervalVector> &solverSolutions);

};
}
#endif /* SRC_KINEMATICCHAIN_H_ */
