/*
 * This software is the property of Auctus, the INRIA research team.
 * Please refer to the Licence file of the repository and Authors
 */

/**
 * @defgroup Twist Twist
 *
 * @brief Class for twists.
 *
 * @author Joshua Pickard
 * Contact: joshua.pickard@inria.fr
 *
 */

#ifndef SRC_TWIST_H_
#define SRC_TWIST_H_

#include "ibex.h"

namespace kcadl{
class Twist {
	friend class KinematicChain;

struct Vector_3{
	ibex::Interval x;
	ibex::Interval y;
	ibex::Interval z;
};

public:
	/**
	Segment constructor
	 */
	Twist();

	/**
	Segment constructor
	\param V linear component
	\param W angular component
	 */
	Twist(ibex::IntervalVector &V, ibex::IntervalVector &W);

	/**
	Segment constructor
	\param V linear component
	\param W angular component
	 */
	Twist(ibex::Vector &V, ibex::Vector &W);

	/**
	Segment constructor
	\param Vx linear component x
	\param Vy linear component y
	\param Vz linear component z
	\param Wx angular component x
	\param Wy angular component y
	\param Wz angular component z
	 */
	Twist(double Vx, double Vy, double Vz, double Wx, double Wy, double Wz);

	/**
	Segment constructor
	\param Vx linear component x
	\param Vy linear component y
	\param Vz linear component z
	\param Wx angular component x
	\param Wy angular component y
	\param Wz angular component z
	 */
	Twist(ibex::Interval Vx, ibex::Interval Vy, ibex::Interval Vz, ibex::Interval Wx, ibex::Interval Wy, ibex::Interval Wz);

	/**
	Segment constructor
	\param varTwist twist function
	\param allVarsVector values
	 */
	Twist(ibex::Function &varTwist, ibex::IntervalVector &allVarsVector);

	/**
	Segment destructor
	 */
	virtual ~Twist();

	/**
	Get linear components
	\return Interval vector of linear components
	 */
	ibex::IntervalVector GetV();

	/**
	Get angular components
	\return Interval vector of angular components
	 */
	ibex::IntervalVector GetW();

	ibex::IntervalVector twist = ibex::IntervalVector(6); /**< Twist*/
	Vector_3 v; /**< Linear components*/
	Vector_3 w; /**< Angular components*/
	ibex::IntervalMatrix rotationW = ibex::IntervalMatrix(3,3); /**< Rotation matrix*/

	friend std::ostream& operator<<(std::ostream &os, Twist &twist){
		return os << "twist\t" << twist.twist << "\n";
	}

private:
	/**
	Get rotation matrix
	 */
	void GetRotationW();
};
}
#endif /* SRC_TWIST_H_ */
